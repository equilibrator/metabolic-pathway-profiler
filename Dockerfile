FROM python:3.8-slim

MAINTAINER elad.noor@weizmann.ac.il

WORKDIR /app/src

COPY . ./

RUN set -eux \
    && apt-get update \
    && apt-get install --yes \
        build-essential \
    && pip install wheel \
    && pip install -r requirements.txt \
    && rm -rf /root/.cache/pip \
    && apt-get purge --yes \
        build-essential \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

RUN ["python", "-c", "from equilibrator_api import ComponentContribution; cc = ComponentContribution()"]

CMD ["bash"]

