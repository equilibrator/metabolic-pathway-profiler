# SBtab data format for Metabolic Pathway Profiler

The MPP tool uses an [SBtab](https://sbtab.net/) table format as an input to describe a metabolic network model with quantitative data. For a general description of SBtab, see the article Lubitz et al. 2016 (reference below) and the draft Specification in the folder SBtab-specification. To check an SBtab document for syntactic correctness, use the validator at [sbtab.net](https://sbtab.net/).

In the combine archive, all input data for a simulation are contained in a single file model.tsv. The file contains a number of tables with defined formats (details see below).  

In principle, MDF and ECM simulations use the same types of input data, but ECM uses more data. MDF requires data on model structure, thermodynamic constants, metabolite bounds, and fluxes (although only the flux directions are actually taken into account). ECM also requires these data, plus extra data on enzyme kinetic constants and enzyme cost weights. In addition, measured metabolite and enzyme concentration data can be used for validation.

The schema file ``schema-sbtab-mpp.tsv`` is a copy from https://github.com/liebermeister/sbtab-toolkit/blob/master/schema-files/schema-sbtab-toolkit/schema-sbtab-toolkit.sbtab.tsv

The folder ``sbtab-specification`` contains a copy of the general SBtab specification (current draft version 1.2). 

## Table types

The schema for SBtab files used as inputs for MDF and ECM is defined in the file ``SBtab-specification/sbtab-schema-ecm.tsv`` (copy of original file from https://github.com/liebermeister/sbtab-toolkit/tree/master/schema-objtables/sbtab-toolkit-schema_ECM). An overview of the table types is given below.

In the table definitions below, each table is described by its ID and type, followed by the column IDs

### !!TableID='Config' TableType='Config'
``user options; required for MDF and ECM``

!Option  &nbsp; !Value &nbsp; !Comment                  

### !!TableID='Reaction' TableType='Reaction'  
``metabolic reactions; required for MDF and ECM``

!ID &nbsp; !ReactionFormula &nbsp; !Identifiers:kegg.reaction &nbsp; !IsReversible &nbsp; !Gene &nbsp; !NameForPlots

### !!TableID='Compound' TableType='Compound'  
``metabolites; required for MDF and ECM``

!ID   &nbsp; !Name   &nbsp; !Identifiers:kegg.compound   &nbsp; !IsConstant   &nbsp; !NameForPlots  

### !!TableID='Thermodynamics' TableType='Quantity' StandardConcentration='M'  
``thermodynamic constants; required for MDF``

!QuantityType   &nbsp; !Reaction   &nbsp; !Compound   &nbsp; !Value   &nbsp; !Unit

### !!TableID='Parameter' TableType='Quantity'  
``kinetic constants; required for ECM``

!QuantityType   &nbsp; !Reaction   &nbsp; !Compound   &nbsp; !Value   &nbsp; !Unit   &nbsp; !Reaction:Identifiers:kegg.reaction  &nbsp; !Compound:Identifiers:kegg.compound  &nbsp;  !ID

### !!TableID='Layout' TableType='Position'  
``positions for graphics layout; optional``

!Element  &nbsp; !PositionX  &nbsp; !PositionY					

### !!TableID='Flux' TableType='Quantity' Unit='mM/s'  
``metablic fluxes; required for MDF and ECM; in MDF, only the flux signs count``

!QuantityType    &nbsp; !Reaction   &nbsp; !Reaction:Identifiers:kegg.reaction   &nbsp; !Value

### !!TableID='GibbsEnergyOfReaction' TableType='Quantity' Unit='kJ/mol'  
``Gibbs free energies of reaction; required for MDF and ECM``

!QuantityType   &nbsp; !Reaction   &nbsp; !Reaction:Identifiers:kegg.reaction   &nbsp; !Value   &nbsp; !OriginalValue   &nbsp; !ReactionFormula

### !!TableID='ConcentrationConstraint' TableType='Quantity' Unit='mM'  
``metabolite bounds; required for MDF and ECM``

!QuantityType   &nbsp; !Compound   &nbsp; !Compound:Identifiers:kegg.compound   &nbsp; !Concentration:Min   &nbsp; !Concentration:Max 

### !!TableID='EnzymeCostWeight' TableType='Quantity' Unit='ArbitraryUnits'  
``enzyme cost weights; required for ECM``

!QuantityType  &nbsp; !Reaction  &nbsp; !Reaction:Identifiers:kegg.reaction  &nbsp; !Value

### !!TableID='ConcentrationData' TableType='Quantity' Unit='mM'  
``metabolite concentration data for validation; optional``

!QuantityType  &nbsp; !Compound  &nbsp; !Compound:Identifiers:kegg.compound  &nbsp; !Value

### !!TableID='EnzymeData' TableType='Quantity' Unit='mM'  
``enzyme concentration data for validation; optional``

!QuantityType  &nbsp; !Reaction  &nbsp; !Reaction:Identifiers:kegg.reaction  &nbsp; !Value
