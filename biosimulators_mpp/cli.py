"""Command-line script for pathway analysis."""

import argparse
import logging
import warnings
import zipfile

import h5py
import matplotlib.pyplot as plt
import pandas as pd
from equilibrator_pathway import EnzymeCostModel, Pathway, ThermodynamicModel
from path import Path
from sbtab.SBtab import SBtabDocument, SBtabError, SBtabTable


warnings.filterwarnings("ignore", category=FutureWarning)

if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description="Calculate the Max-min Driving Force (MDF) of a pathway."
    )
    parser.add_argument(
        "-d", "--debug", action="store_true", help="full application debug mode"
    )
    parser.add_argument(
        "-q", "--quiet", action="store_true", help="suppress all console output"
    )
    parser.add_argument(
        "-i",
        "--archive",
        type=str,
        required=True,
        help="ARCHIVE\nPath to a COMBINE/OMEX archive file which contains one or more SED-ML-encoded simulation experiments",
    )
    parser.add_argument(
        "-o",
        "--out-dir",
        type=str,
        required=True,
        help="OUT_DIR\nDirectory to save outputs",
    )
    parser.add_argument(
        "-v",
        "--version",
        action="store_true",
        help="show program's version number and exit",
    )

    args = parser.parse_args()

    if args.debug:
        logging.getLogger().setLevel(logging.DEBUG)
    elif args.quiet:
        logging.getLogger().setLevel(logging.ERROR)
    else:
        logging.getLogger().setLevel(logging.WARNING)

    # open the COMPINE/OMEX archive file (which is a standard .zip file)
    with zipfile.ZipFile(args.archive) as archive:
        assert (
            "model.tsv" in archive.namelist()
        ), "input COMBINE file must contain a file called 'model.tsv'"

        sbtabdoc = SBtabDocument(
            "pathway",
            archive.open("model.tsv").read().decode(encoding="utf-8"),
            "model.tsv",
        )

    output_dir = Path(args.out_dir)

    _config_sbtab = sbtabdoc.get_sbtab_by_id("Configuration")
    config_df = _config_sbtab.to_data_frame().set_index("Option")
    optimization_method = config_df.Value["algorithm"]

    if optimization_method == "MDF":
        pathway = ThermodynamicModel.from_sbtab(sbtabdoc)
        if pathway.Nr == 0:
            raise ValueError("Empty pathway")
        logging.info("Starting pathway analysis")
        analysis_results = pathway.mdf_analysis()
    elif optimization_method == "ECM":
        ecm_model = EnzymeCostModel.from_sbtab(sbtabdoc)
        pathway = ecm_model._thermo_model
        if pathway.Nr == 0:
            raise ValueError("Empty pathway")
        analysis_results = ecm_model.optimize_ecm()
    else:
        raise ValueError("Input SBtab file does not qualify as MDF nor ECM model")
    logging.info("Finished pathway analysis")

    # create the output files (HDF5 and zipped PDFs)

    reaction_table = analysis_results.reaction_df.copy()

    # the 'flux' and 'dg' values in the DataFrame are not standard floats but `pint`
    # objects, so we need to downcast them to floats in specific units (e.g. kJ/mol)
    reaction_table["flux"] = reaction_table.flux.map(lambda g: g.m_as("mM/s"))
    for dg_type in ["original_standard", "standard", "physiological", "optimized"]:
        col = dg_type + "_dg_prime"
        reaction_table[col] = reaction_table[col].map(lambda g: g.m_as("kJ/mol"))

    # format the compound table
    compound_table = analysis_results.compound_df.copy()
    compound_table = compound_table.loc[compound_table.compound_id != "H2O", :]
    compound_table["concentration"] = compound_table.concentration.map(
        lambda g: g.m_as("mM")
    )
    compound_table["lower_bound"] = compound_table.lower_bound.map(
        lambda g: g.m_as("mM")
    )
    compound_table["upper_bound"] = compound_table.upper_bound.map(
        lambda g: g.m_as("mM")
    )

    with pd.HDFStore(output_dir / "reports.h5", "w") as output_h5_file:
        output_h5_file.put(
            "reactions", reaction_table, format="table", data_columns=True
        )
        output_h5_file.put(
            "compounds", compound_table, format="table", data_columns=True
        )

    with zipfile.ZipFile(output_dir / "plots.zip", "w") as output_zip_file:
        if optimization_method == "MDF":
            fig1, ax1 = plt.subplots(
                1,
                1,
                figsize=(8, 2 + 0.2 * analysis_results.compound_df.shape[0]),
                dpi=90,
            )
            analysis_results.plot_concentrations(ax1)
            fig1.tight_layout()
            with output_zip_file.open("concentration.pdf", "w") as conc_fig_file:
                fig1.savefig(conc_fig_file, format="pdf")
            fig2, ax2 = plt.subplots(1, 1, figsize=(8, 6), dpi=90)
            analysis_results.plot_driving_forces(ax2)
            fig2.tight_layout()
            with output_zip_file.open("driving_forces_mdf.pdf", "w") as df_fig_file:
                fig2.savefig(df_fig_file, format="pdf")
        elif optimization_method == "ECM":
            fig1, ax1 = plt.subplots(1, 1, figsize=(8, 6), dpi=90)
            analysis_results.plot_thermodynamic_profile(ax1)
            fig1.tight_layout()
            with output_zip_file.open("driving_forces_ecm.pdf", "w") as tp_fig_file:
                fig1.savefig(tp_fig_file, format="pdf")
            fig2, ax2 = plt.subplots(1, 1, figsize=(8, 6), dpi=90)
            analysis_results.plot_enzyme_demand_breakdown(ax2, plot_measured=False)
            fig2.tight_layout()
            with output_zip_file.open(
                "enzyme_demand_breakdown.pdf", "w"
            ) as edb_fig_file:
                fig2.savefig(edb_fig_file, format="pdf")
        else:
            raise ValueError("Input SBtab file does not qualify as MDF nor ECM model")
