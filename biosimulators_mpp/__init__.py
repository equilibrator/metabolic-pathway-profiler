import equilibrator_pathway

from ._version import __version__  # noqa: F401
from .core import exec_sed_task  # noqa: F401
from .core import exec_sed_doc, exec_sedml_docs_in_combine_archive


# :obj:`str`: version


__all__ = [
    "__version__",
    "get_simulator_version",
    "exec_sed_task",
    "exec_sed_doc",
    "exec_sedml_docs_in_combine_archive",
]


def get_simulator_version():
    """Get the version of eQuilibrator

    Returns:
        :obj:`str`: version
    """
    return equilibrator_pathway.__version__
