""" BioSimulators-compliant command-line interface to `MPA <https://equilibrator.weizmann.ac.il/pathway/>`_ - Metabolic Pathway Analysis software

:Author: Elad Noor <elad.noor@weizmann.ac.il>, Wolfram Liebermeister <wolfram.liebermeister@gmail.com>
:Date: March 23, 2022
:Copyright: 2022, Weizmann Institute of Science & INRAE
:License: MIT
"""

from biosimulators_utils.simulator.cli import build_cli

from . import get_simulator_version
from ._version import __version__
from .core import exec_sedml_docs_in_combine_archive


App = build_cli(
    "biosimulators-mpa",
    __version__,
    "equilibrator_pathway",
    get_simulator_version(),
    "https://equilibrator.weizmann.ac.il/pathway/",
    exec_sedml_docs_in_combine_archive,
)


def main():
    with App() as app:
        app.run()
